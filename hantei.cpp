#include"DxLib.h"
#include <string.h>

void bs_hantei(int mouse_x,int mouse_y,int *b_mouse_click,FILE *answer,FILE *answer_a,char for_fix[2010],int key[256]){
	
	int size;
	unsigned int blue;
	blue = GetColor(0, 143, 232);

	if ((((870 < mouse_x) && (970 > mouse_x)) && ((10 < mouse_y) && (100 > mouse_y))) || key[KEY_INPUT_BACK]==1){
		DrawBox(865, 5, 975, 115, blue, TRUE);
		if ((*b_mouse_click != 0 && (GetMouseInput() & MOUSE_INPUT_LEFT) == 0) || key[KEY_INPUT_BACK] == 1){
			//answerの読み込み
			fseek(answer, 0, SEEK_SET);
			
			//----------------

			//もしanswerに文字が入っているなら
			//文字列の最後に\0を入れる
			if (fgets(for_fix, sizeof(int) * 2010, answer) != NULL){
				size = strlen(for_fix);
				if (size >= 1){
					for_fix[size - 1] = '\0';
					//一度ファイルを閉じて初期化する
					fclose(answer);
					fclose(answer_a);
					//開きなおす
					fopen_s(&answer, "output/answer.txt", "w+");
					fopen_s(&answer_a, "output/answer_a.txt", "a");
					//\0を代入した文字列をファイルに出力する
					fprintf(answer, "%s", for_fix);
					fprintf(answer_a, "\n//-----------------------------------------------\n\n");
					fprintf(answer_a, "%s", for_fix);
				}
			}
		
		}
		//押されているかどうかの判定
		if ((GetMouseInput() & MOUSE_INPUT_LEFT) != 0){
			*b_mouse_click = 1;
		}
		else{
			*b_mouse_click = 0;
		}

	}else{
		*b_mouse_click = 0;
	}
}

void en_hantei(int mouse_x,int mouse_y,int *e_mouse_click,int *p_sum
	,FILE *answer,FILE *output_r,FILE *answer_t,FILE *answer_t_a,int key[256],int nikaime_hantei){
		char onetime[2010];
		unsigned int blue;

		blue = GetColor(0, 143, 232);

		if (((((890 < mouse_x) && (990 > mouse_x)) && ((170 < mouse_y) && (270 > mouse_y)))
			|| key[KEY_INPUT_RETURN] == 1)&&(nikaime_hantei==7)){
			DrawBox(885,165,995,275,blue, TRUE);
			if ((*e_mouse_click != 0 && (GetMouseInput() & MOUSE_INPUT_LEFT) == 0) || key[KEY_INPUT_RETURN] == 1){

					fseek(answer,0,SEEK_SET);
					if(fgets(onetime,sizeof onetime,answer)!=NULL){
						fopen_s(&answer_t_a, "output/true_answer_a.txt", "a");

						fprintf(answer_t,"%s",onetime);
						fprintf(answer_t_a,"%s",onetime);

						fclose(answer_t_a);
						fclose(answer);
						fclose(output_r);
						fopen_s(&output_r,"output/output_r.txt","w+");
						fopen_s(&answer, "output/answer.txt", "w+");
						(*p_sum)++;
						
					}
				}
				//押されているかどうかの判定
				if ((GetMouseInput() & MOUSE_INPUT_LEFT) != 0){
					*e_mouse_click = 1;
				}
				else{
					*e_mouse_click = 0;
				}
		}else{
			*e_mouse_click = 0;
		}
}