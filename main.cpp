#include "DxLib.h"
#include "saikoro.h"
#include "hantei.h"

#define SIZE_X 1000
#define SIZE_Y 700

int updatekey(int *key);

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int){
	//---------------------------------------------------------------------------
	ChangeWindowMode(TRUE); // ウィンドウモードに設定
	DxLib_Init();   // DXライブラリ初期化処理
	SetWindowText("さいころぐらし！　〜さいころ入力じーゆーあい〜");
	SetDrawScreen(DX_SCREEN_BACK);
	SetMouseDispFlag(TRUE);
	SetGraphMode(SIZE_X,SIZE_Y,32);
	//---------------------------------------------------------------------------

	//------------------宣言部--------------------------
	//---テーブルルール読み込み用---
	int number_1,number_2;
	char rule[3][6][7],input_c,size_1,size_2,saikoro_filename[256];

	//------画像読み込み用------
	int saikoro[6],saikoro_low[6],backspace,enter;

	//-------さいころの座標-------
	int saikoro_x[2][2][6], saikoro_y[2][2][6];
	//saikoro_(x or y)で0は大きいサイコロ　1は普通のサイコロ　2は小さなサイコロ

	//-------マウスの座標---------
	int mouse_x, mouse_y;

	//--------ループカウンタ--------
	int i,j;

	//表示のための配列と修正のための配列
	char str[256],str_a[256],for_fix[2010];

	//-----マウスのクリック判定用-------
	int mouse_click[3][6]={0},b_mouse_click,e_mouse_click;

	//--------表示のx,y座標用-----------
	int gyou,retu;

	//-----キーボード入力--------------
	int key[256];
	//----------合計入力数------------
	int s_sum = 0,p_sum=0;

	//----サイコロ入力のフラグとか----
	int nikaime_hantei = 7,c_flag=0,m_flag=0;
	char h_answer[8];
	
	//----ファイルポインタ達--------
	FILE *output,*output_a,*output_r;
	FILE *answer,*answer_a,*answer_t,*answer_t_a/*true_answer*/;
	FILE *table_rule;

	//-------いろいろな色-----------
	unsigned int blue,white,black,green,orange,brown;
	//--------------------------------------------------


	//-------------------いろいろ-----------------------
	white=GetColor(255,255,255);
	blue= GetColor(0, 143, 232);
	black=GetColor(0,0,0);
	orange = GetColor(255,142,0);
	green = GetColor(0,255,0);
	brown=GetColor(110,50,0);
	//--------------------------------------------------
	
	//-----ファイルを開きまくって圧倒的成長-------------
	fopen_s(&table_rule,"input/table_rule.txt","r");
	fopen_s(&output,"output/output.txt","w+");
	fopen_s(&output_a,"output/output_a.txt","a");
	fopen_s(&output_r,"output/output_r.txt","w+");
	fopen_s(&answer, "output/answer.txt", "w+");
	fopen_s(&answer_a, "output/answer_a.txt", "a");
	fopen_s(&answer_t, "output/true_answer.txt", "w");
	//fopen_s(&answer_t_a, "output/true_answer_a.txt", "a");

	if((table_rule==NULL)||(answer==NULL)||(answer_a==NULL)||(answer_t==NULL)
		||(output==NULL)||(output_a==NULL)||(output_r==NULL)){
		return -1;
	}
	//--------------------------------------------------

	//------追加書き込みファイルに点線を挿入-----------
	fprintf(output_a,"//-----------------------------------------------\n\n");
	fprintf(answer_a, "\n//-----------------------------------------------\n\n");
	//fprintf(answer_t_a, "\n//-----------------------------------------------\n\n");
	//--------------------------------------------------
	
	//------------テーブルルールを格納---------------
	for(i=0;i<108;i++){
		fgets(str,sizeof str,table_rule);
		sscanf_s(str,"%c%d%c%d %c",&size_1,sizeof size_1,&number_1,&size_2,sizeof size_2,&number_2,&input_c);
		if((size_1=='b')&&(size_2=='b')){
			rule[0][number_1][number_2]=input_c;
		}else if((size_1=='n')&&(size_2=='n')){
			rule[1][number_1][number_2]=input_c;
		}else if((size_1=='b')&&(size_2=='n')){
			rule[2][number_1][number_2]=input_c;
		}
		
		
	}
	//--------------------------------------------------

	//-------------------画像読み込み--------------------
	//---------さいころの画像--------
	for (i = 1; i <= 6; i++){
		sprintf_s(saikoro_filename, "pic/saikoro(high)/%d.png", i);//sprintf→sprintf_s
		saikoro[i-1]=LoadGraph(saikoro_filename);
		sprintf_s(saikoro_filename, "pic/saikoro/%d.png", i);//sprintf→sprintf_s
		saikoro_low[i - 1] = LoadGraph(saikoro_filename);

	}

	//--------削除ボタン-------------
	backspace = LoadGraph("pic/bs.png");
	//-------決定ボタン--------------
	enter=LoadGraph("pic/enter.png");
	
	//--------------------------------------------------



	//----------さいころの座標の初期化--------
	saikoro_initialize(0,0,160,10,10,0,saikoro_x,saikoro_y);
	saikoro_initialize(520,0,100,10,10,1,saikoro_x,saikoro_y);
	//----------------------------------------

	//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	//*****************************************************************************************************
	//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0 &&updatekey(key)==0){
		GetMousePoint(&mouse_x, &mouse_y);
		

		//下の白四角
		DrawBox(0, 670, 1000, 700, white, TRUE);
		
		//-----パケットの表示-----
		DrawBox(520, 280,520+30+148*3,280+30+102*3,brown, TRUE);
		DrawBox(535, 295,535+148*3,295+102*3,black, TRUE);
		//-------------------------

		

		


		//---------------------描画部-----------------------
		//選択枠の表示
		saikoro_choose(mouse_x, mouse_y, saikoro_x, saikoro_y, mouse_click, &s_sum,
			output, output_a,output_r,answer,answer_a,
			&nikaime_hantei,h_answer,rule,&c_flag);

		saikoro_choose_key(key,&s_sum,output,output_a,output_r,answer,answer_a,&nikaime_hantei,h_answer,rule,&c_flag);

		bs_hantei(mouse_x, mouse_y, &b_mouse_click, answer, answer_a,for_fix,key);
		if(c_flag==1){
			en_hantei(mouse_x,mouse_y,&e_mouse_click,&p_sum,answer,output_r,answer_t,answer_t_a,key,nikaime_hantei);
		}
		//-------------

		//-----さいころ1〜6までの描画--------
		for (i = 0; i < 2; i++){
			for (j = 0; j < 6; j++){
				DrawExtendGraph(saikoro_x[i][0][j], saikoro_y[i][0][j], saikoro_x[i][1][j], saikoro_y[i][1][j], saikoro[j], FALSE);
			}
		}
		
		//-----------------------------------
		DrawExtendGraph(870, 10, 970, 110, backspace, FALSE);
		//-----------------------------------
		DrawExtendGraph(890,170,990,270,enter,FALSE);
		//-----------------------------------
		DrawFormatString(0, 680, black, "合計入力数:%dサイコロ,%dパケット", s_sum,p_sum);
		//--------------------------------------------------
		

		//------output_rの中身の表示-------------
		fseek(output_r, 0, SEEK_SET);
		gyou = 350;
		retu = 10;
		while (fgets(str, sizeof str, output_r) != NULL){
			
			DrawFormatString(retu, gyou, green, str);

			//-------
			//削除機能実装後に　ここにパケット表示
			//-------

			gyou += 20;
			if (gyou > 620){
				retu += 70;
				gyou = 350;
			}
		}
		fseek(output_r, 0, SEEK_END);

		//------------------------------------------

		//----------answerの中身表示------------
		if (c_flag == 1){
			fseek(answer, 0, SEEK_SET);
			if(fgets(str_a, sizeof str_a, answer)!=NULL){
				DrawFormatString(10, 640, green, str_a);
				Drawp(saikoro_low, rule, str_a);
			}
			fseek(answer, 0, SEEK_END);
			
		}
		
		//-------終了時点でanswerはSEEK_END-----
		
		
		
		
		//マウス座標の表示
		DrawFormatString(mouse_x + 5, mouse_y + 5, orange, "x=%d,y=%d", mouse_x, mouse_y);
	}
	
	fprintf(answer_a, "\n//---------↑さっきの最後はこれ↑------------\n\n");

	//-----ファイルを閉じまくってファイルに圧倒的感謝-----
	fclose(output_a);
	fclose(output);
	fclose(output_r);
	fclose(answer_t);
	//fclose(answer_t_a);
	fclose(answer_a);
	fclose(answer);
	fclose(table_rule);
	//----------------------------------------------------

	DxLib_End();    // DXライブラリ終了処理
	return 0;
}


int updatekey(int *key){
	char key_u[256];
	int i;

	GetHitKeyStateAll(key_u);
	for (i = 0; i < 256; i++){
		if (key_u[i] != 0){
			key[i]++;
		}
		else{
			key[i] = 0;
		}

	}


	return 0;
}


/*カレントディレクトリ関係の残骸
	char carent[256];

	GetCurrentDirectory(256, carent);
		DrawFormatString(0,0,blue,carent);


*/

/*残骸

		/*----------outputの中身表示------------
		fseek(output, 0, SEEK_SET);
		gyou = 350;
		retu = 10;
		while (fgets(str, sizeof str, output) != NULL){
			
			DrawFormatString(retu, gyou, green, str);
			gyou += 20;
			if (gyou > 620){
				retu += 50;
				gyou = 350;
			}
		}
		fseek(output, 0, SEEK_END);
		//------終了時点でoutputはSEEK_END------
*/