#include "DxLib.h"
#include <string.h>
#define WAKU 5
//-----------------------
void saikoro_c_k_output(int num, int bn_flag, int *s_sum,
	FILE *output, FILE *output_a, FILE *output_r, FILE *answer, FILE *answer_a,
	int *nikaime_hantei, char h_answer[8], char(*rule)[6][7], int *c_flag);
//-------------------------
void saikoro_initialize(int saikoro_start_x,int saikoro_start_y,int saikoro_size
	,int saikoro_space_x,int saikoro_space_y,int mode,int (*saikoro_x)[2][6],int (*saikoro_y)[2][6]){
	//--------------さいころのx,y座標の決定-------------
	//0→(xの場合は右の座標,yの場合は上の座標)
	//1→(xの場合は左の座標,yの場合は下の座標)
	int saikoro_number;
	int i, j;
	int x=saikoro_start_x, y=saikoro_start_y;
	saikoro_number = 0;
	for (i = 0; i < 2; i++){
		x = saikoro_start_x;
		y += saikoro_space_y;
		for (j = 0; j < 3; j++){
			x += saikoro_space_x;
			saikoro_x[mode][0][saikoro_number] = x;
			saikoro_y[mode][0][saikoro_number] = y;
			saikoro_x[mode][1][saikoro_number] = x+saikoro_size;
			saikoro_y[mode][1][saikoro_number] = y+saikoro_size;

			x += saikoro_size;
			saikoro_number++;
		}
		y += saikoro_size;
	}
	//--------------------------------------------------
	
}

void Drawp(int saikoro_low[6],char (*rule)[6][7],char str[256]){
	int gyou=295,retu=535,B_flag=0;//大サイコロ判定用
	int i,j,k,nagasa;

	nagasa=strlen(str);

	for(i=0;i<nagasa;i++){
		for(j=0;j<6;j++){
			for(k=0;k<6;k++){
				if(str[i]==rule[0][j][k]){
					if(retu+16*3*2>979){
						if(B_flag==1){
							gyou+=16*3;
						}else{
							gyou+=10*3;
						}
						retu=535;
						B_flag=0;
					}
					DrawExtendGraph(retu,gyou,retu+16*3,gyou+16*3,saikoro_low[j],FALSE);
					retu+=16*3;
					DrawExtendGraph(retu,gyou,retu+16*3,gyou+16*3,saikoro_low[k],FALSE);
					retu+=16*3;
					B_flag=1;
				}else if(str[i]==rule[1][j][k]){
					if(retu+10*3*2>979){
						if(B_flag==1){
							gyou+=16*3;
						}else{
							gyou+=10*3;
						}
						retu=535;
						B_flag=0;
					}
					DrawExtendGraph(retu,gyou,retu+10*3,gyou+10*3,saikoro_low[j],FALSE);
					retu+=10*3;
					DrawExtendGraph(retu,gyou,retu+10*3,gyou+10*3,saikoro_low[k],FALSE);
					retu+=10*3;
				}else if(str[i]==rule[2][j][k]){
					if(retu+16*3+10*3>979){
						if(B_flag==1){
							gyou+=16*3;
						}else{
							gyou+=10*3;
						}
						retu=535;
						B_flag=0;
					}
					DrawExtendGraph(retu,gyou,retu+16*3,gyou+16*3,saikoro_low[j],FALSE);
					retu+=16*3;
					DrawExtendGraph(retu,gyou,retu+10*3,gyou+10*3,saikoro_low[k],FALSE);
					retu+=10*3;
					B_flag=1;
				}
			}
		}
			
	}
}

void saikoro_choose(int mouse_x,int mouse_y,int (*saikoro_x)[2][6],int (*saikoro_y)[2][6],int m_c[3][6],int *s_sum
	,FILE *output,FILE *output_a,FILE *output_r,FILE *answer,FILE *answer_a,
	int *nikaime_hantei,char h_answer[8],char (*rule)[6][7],int *c_flag){
	int i,j,b=0;
	unsigned int red,blue;
	int gyou = 200;
	red = GetColor(255,0,0);
	blue = GetColor(0,143,232);
	
	for(i=0;i<2;i++){
		for(j=0;j<6;j++){
			
			if(((saikoro_x[i][0][j]<mouse_x)&&(saikoro_x[i][1][j]>mouse_x))&&
				((saikoro_y[i][0][j]<mouse_y)&&(saikoro_y[i][1][j]>mouse_y))){
					switch(i){
						case 0:
								DrawFormatString(950,680,red,"大,%d",j+1);
						break;

						case 1:
								 DrawFormatString(950,680,red,"中,%d",j+1);
								
						break;
					}
					
					DrawBox(saikoro_x[i][0][j]-WAKU,saikoro_y[i][0][j]-WAKU,saikoro_x[i][1][j]+WAKU,saikoro_y[i][1][j]+WAKU,blue,TRUE);
					//----------------------------
					
					 if( m_c[i][j]!=0 &&( GetMouseInput() & MOUSE_INPUT_LEFT ) == 0 ){
						 //
						 (*s_sum)++;
						 switch(i){
							 case 0:
								 if(*nikaime_hantei==7){
									fprintf(output,"b%d",j);
									fprintf(output_a, "b%d", j);
									//*************************
									//output_rだけは数字を+1して出力
									fprintf(output_r,"b%d",j+1);
									//*************************
									sprintf_s(h_answer, sizeof h_answer, "b%d", j);
									*nikaime_hantei=j;
									//*c_flag=0;
								 }else if(*nikaime_hantei!=7){

									fprintf(output,"b%d\n",j);
									fprintf(output_a,"b%d\n",j);
									fprintf(output_r,"b%d",j+1);

									if (h_answer[0] == 'b'&&rule[0][*nikaime_hantei][j] != '['){

										fprintf(output_r,"|%c|\n",rule[0][*nikaime_hantei][j]);
										
										fprintf(answer, "%c", rule[0][*nikaime_hantei][j]);
										
										fprintf(answer_a, "%c", rule[0][*nikaime_hantei][j]);
										
									}
									else{
										fprintf(output_r,"| |\n");
									}
									*c_flag = 1;
									*nikaime_hantei=7;
									

								 }
							 break;

							 case 1:
								 if(*nikaime_hantei==7){
									fprintf(output,"n%d",j);
									fprintf(output_a,"n%d",j);
									fprintf(output_r,"n%d",j+1);
									sprintf_s(h_answer, sizeof h_answer, "n%d", j);
									*nikaime_hantei=j;
									//*c_flag=0;
								 }else if(*nikaime_hantei!=7){
									 fprintf(output,"n%d\n",j);
									 fprintf(output_a,"n%d\n",j);
									 fprintf(output_r,"n%d",j+1);

									 if (h_answer[0] == 'n'&&rule[1][*nikaime_hantei][j] != '['){
										 
										fprintf(answer, "%c", rule[1][*nikaime_hantei][j]);
										fprintf(answer_a, "%c", rule[1][*nikaime_hantei][j]);
										 fprintf(output_r, "|%c|\n", rule[1][*nikaime_hantei][j]);

									 }
									 else if (h_answer[0] == 'b'&&rule[2][*nikaime_hantei][j] != '['){
										 fprintf(answer, "%c", rule[2][*nikaime_hantei][j]);
										 fprintf(answer_a, "%c", rule[2][*nikaime_hantei][j]);
										 fprintf(output_r, "|%c|\n", rule[2][*nikaime_hantei][j]);
									 }else{
										 fprintf(output_r,"| |\n");
									 }

									*c_flag = 1;
									*nikaime_hantei=7;
									
								 }
								
							 break;

							 
						 }
					 }
					 if ((GetMouseInput() & MOUSE_INPUT_LEFT) != 0){
						 m_c[i][j] = 1;
					 }
					 else{
						 m_c[i][j] = 0;
					 }
					//----------------------------

				break;
			}
			else{
				m_c[i][j] = 0;
			}
			
		}
	}
}

void saikoro_choose_key(int key[256], int *s_sum,
	FILE *output, FILE *output_a, FILE *output_r, FILE *answer, FILE *answer_a,
	int *nikaime_hantei, char h_answer[8], char(*rule)[6][7], int *c_flag){
	
	unsigned int blue;
	int bn_flag=0;//1==b

	blue = GetColor(0, 143, 232);

	if (key[KEY_INPUT_B] != 0/* || key[KEY_INPUT_RSHIFT] != 0*/){
		bn_flag = 1;
	}
	//530 235
	switch (bn_flag){
	case 0:
		DrawFormatString(530, 235, blue, "中");
		break;
	case 1:
		DrawFormatString(530, 235, blue, "大");
		break;
	}
	//-----------------------------------------------
	if (key[KEY_INPUT_NUMPAD1] == 1 || key[KEY_INPUT_1] == 1){
		saikoro_c_k_output(0, bn_flag,s_sum,
			output, output_a, output_r, answer, answer_a,
			nikaime_hantei, h_answer, rule, c_flag);
	}
	else if (key[KEY_INPUT_NUMPAD2] == 1 || key[KEY_INPUT_2] == 1){
		saikoro_c_k_output(1, bn_flag, s_sum,
			output, output_a, output_r, answer, answer_a,
			nikaime_hantei, h_answer, rule, c_flag);
	}
	else if (key[KEY_INPUT_NUMPAD3] == 1 || key[KEY_INPUT_3] == 1){
		saikoro_c_k_output(2, bn_flag, s_sum,
			output, output_a, output_r, answer, answer_a,
			nikaime_hantei, h_answer, rule, c_flag);
	}
	else if (key[KEY_INPUT_NUMPAD4] == 1 || key[KEY_INPUT_4] == 1){
		saikoro_c_k_output(3, bn_flag, s_sum,
			output, output_a, output_r, answer, answer_a,
			nikaime_hantei, h_answer, rule, c_flag);
	}
	else if (key[KEY_INPUT_NUMPAD5] == 1 || key[KEY_INPUT_5] == 1){
		saikoro_c_k_output(4, bn_flag, s_sum,
			output, output_a, output_r, answer, answer_a,
			nikaime_hantei, h_answer, rule, c_flag);
	}
	else if (key[KEY_INPUT_NUMPAD6] == 1 || key[KEY_INPUT_6] == 1){
		saikoro_c_k_output(5, bn_flag, s_sum,
			output, output_a, output_r, answer, answer_a,
			nikaime_hantei, h_answer, rule, c_flag);
	}

}

void saikoro_c_k_output(int num, int bn_flag,int *s_sum, 
	FILE *output, FILE *output_a, FILE *output_r, FILE *answer, FILE *answer_a,
	int *nikaime_hantei, char h_answer[8], char(*rule)[6][7], int *c_flag){

	(*s_sum)++;

	switch (bn_flag){
	case 1:
		if (*nikaime_hantei == 7){
			fprintf(output, "b%d", num);
			fprintf(output_a, "b%d", num);
			//*************************
			//output_rだけは数字を+1して出力
			fprintf(output_r, "b%d", num + 1);
			//*************************
			sprintf_s(h_answer, sizeof h_answer, "b%d", num);
			*nikaime_hantei = num;
			//*c_flag=0;
		}
		else if (*nikaime_hantei != 7){

			fprintf(output, "b%d\n", num);
			fprintf(output_a, "b%d\n", num);
			fprintf(output_r, "b%d", num + 1);

			if (h_answer[0] == 'b'&&rule[0][*nikaime_hantei][num] != '['){

				fprintf(output_r, "|%c|\n", rule[0][*nikaime_hantei][num]);

				fprintf(answer, "%c", rule[0][*nikaime_hantei][num]);

				fprintf(answer_a, "%c", rule[0][*nikaime_hantei][num]);

			}
			else{
				fprintf(output_r, "| |\n");
			}
			*c_flag = 1;
			*nikaime_hantei = 7;


		}
		break;

	case 0:
		if (*nikaime_hantei == 7){
			fprintf(output, "n%d", num);
			fprintf(output_a, "n%d", num);
			fprintf(output_r, "n%d", num + 1);
			sprintf_s(h_answer, sizeof h_answer, "n%d" ,num);
			*nikaime_hantei = num;
			//*c_flag=0;
		}
		else if (*nikaime_hantei != 7){
			fprintf(output, "n%d\n", num);
			fprintf(output_a, "n%d\n", num);
			fprintf(output_r, "n%d", num + 1);

			if (h_answer[0] == 'n'&&rule[1][*nikaime_hantei][num] != '['){

				fprintf(answer, "%c", rule[1][*nikaime_hantei][num]);
				fprintf(answer_a, "%c", rule[1][*nikaime_hantei][num]);
				fprintf(output_r, "|%c|\n", rule[1][*nikaime_hantei][num]);

			}
			else if (h_answer[0] == 'b'&&rule[2][*nikaime_hantei][num] != '['){
				fprintf(answer, "%c", rule[2][*nikaime_hantei][num]);
				fprintf(answer_a, "%c", rule[2][*nikaime_hantei][num]);
				fprintf(output_r, "|%c|\n", rule[2][*nikaime_hantei][num]);
			}
			else{
				fprintf(output_r, "| |\n");
			}

			*c_flag = 1;
			*nikaime_hantei = 7;

		}

		break;


	}

}

//↓mainの元ソース
/*--------------さいころのx,y座標の決定------------- 
	//0→(xの場合は右の座標,yの場合は上の座標)
	//1→(xの場合は左の座標,yの場合は下の座標)
	saikoro_size = 100;//さいころのサイズ
	saikoro_space = 30;//さいころの間隔
	saikoro_number = 0;
	for (i = 0; i < 2; i++){
		x = 0;
		y += saikoro_space;
		for (j = 0; j < 3; j++){
			x += saikoro_space;
			saikoro_x[0][saikoro_number] = x;
			saikoro_y[0][saikoro_number] = y;
			saikoro_x[1][saikoro_number] = x+saikoro_size;
			saikoro_y[1][saikoro_number] = y+saikoro_size;

			x += saikoro_size;
			saikoro_number++;
		}
		y += saikoro_size;
	}

	*///--------------------------------------------------

//saikoro_chooseの残骸
/*int s_x_b[][6],int s_y_b[][6]
		,int s_x_n[][6],int s_y_n[][6],int s_x_s[][6],int s_y_s[][6]*/
/*
			if(((s_x_n[0][i]<mouse_x)&&(s_x_n[1][i]>mouse_x))&&
				((s_y_n[0][i]<mouse_y)&&(s_y_n[1][i]>mouse_y))){
					DrawFormatString(600,0,color,"ふつう,%d",i+1);
					DrawBox(s_x_n[0][i]-WAKU,s_y_n[0][i]-WAKU,s_x_n[1][i]+WAKU,s_y_n[1][i]+WAKU,color,TRUE);
				break;
			}
			if(((s_x_s[0][i]<mouse_x)&&(s_x_s[1][i]>mouse_x))&&
				((s_y_s[0][i]<mouse_y)&&(s_y_s[1][i]>mouse_y))){
					DrawFormatString(600,0,color,"小さい,%d",i+1);
					DrawBox(s_x_s[0][i]-WAKU,s_y_s[0][i]-WAKU,s_x_s[1][i]+WAKU,s_y_s[1][i]+WAKU,color,TRUE);
				break;
			}
			*/

//小さいころの残骸
/*case 2:
if(*nikaime_hantei==0){
fprintf(output,"s%d",j);
*nikaime_hantei=1;
}else if(*nikaime_hantei==1){
fprintf(output,"s%d\n",j);
*nikaime_hantei=0;
}

break;
*/