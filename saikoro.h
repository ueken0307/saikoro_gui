#ifndef SAIKORO_H

#define SAIKORO_H

void saikoro_initialize(int saikoro_start_x,int saikoro_start_y,int saikoro_size, int saikoro_space_x,int saikoro_space_y,int mode,int (*saikoro_x)[2][6], int (*saikoro_y)[2][6]);

void saikoro_choose(int mouse_x, int mouse_y, int(*saikoro_x)[2][6], int(*saikoro_y)[2][6], int m_c[3][6], int *s_sum
	, FILE *output, FILE *output_a,FILE *output_r, FILE *answer, FILE *answer_a,
	int *nikaime_hantei, char h_answer[8], char(*rule)[6][7],int *c_flag);

void Drawp(int saikoro_low[6],char (*rule)[6][7],char str[256]);

void saikoro_choose_key(int key[256], int *s_sum,
	FILE *output, FILE *output_a, FILE *output_r, FILE *answer, FILE *answer_a,
	int *nikaime_hantei, char h_answer[8], char(*rule)[6][7], int *c_flag);

#endif